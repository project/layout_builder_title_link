# Layout Builder Title Link
## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

The Layout Builder Title Link module provides a new URL field to the block
configuration form in layout builder to overrides the block title field
and makes it linkable, regardless of the block type the URL field will
appear for all block types.

This module provides a new file name suggestion TWIG for the block called
block--layout-builder-title-link.html.twig.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/issues/layout_builder_title_link

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/layout_builder_title_link


## REQUIREMENTS

This module requires no modules outside of Drupal core.
To this module you need to install the Layout Builder module.


## RECOMMENDED MODULES

Enable Drupal core Layout Builder module.


## INSTALLATION

The installation of this module is like other Drupal modules,
install the module and enable it.


Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


## CONFIGURATION

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add a new URL field to the block
configuration form in layout builder.

## MAINTAINERS

Current maintainers for Drupal 10:

- **Mohammad Fayoumi** (mohammad-fayoumi) - [https://www.drupal.org/u/mohammad-fayoumi](https://www.drupal.org/u/mohammad-fayoumi)
- **Vardot** (vardot) - [https://www.drupal.org/vardot](https://www.drupal.org/vardot)
